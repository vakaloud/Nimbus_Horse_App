package com.nimbus.testgethorse;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import static java.lang.Math.abs;
import static java.lang.Math.ceil;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static org.opencv.core.CvType.CV_8UC1;

/*
 * This is an implementation for Android of the DaDa_Detector code developed in Java for computer use.
 * It displays a picture of a horse on screen with a rectangle by default.
 * The user has to resize the rectangle as tightly as possible around the horse and click ok.
 * Then GrabCut is run, and we can visualize what was kept as possible foreground pixels.
 * Then the remaining image goes through GaussianBLur and Canny and we get contours.
 * We can compare contours with a reference contour.
 * If we know enough parameters, we can apply triangle similarity to get the horse's dimensions.
 *
 * This process can be refined. Tests have shown that the biggest contour is not necesserally of
 * the horse, and a contour around possible foreground pixels after grabCut can contain other
 * elements giving "false negative" results on comparison with a reference outline.
 */

public class MainActivity extends AppCompatActivity {
    static {
        if(!OpenCVLoader.initDebug()){}
    }

    static Mat imag = null;
    private static String TAG = "MainActivity";
    ImageView cutOut;
    ImageView contours;
    DrawView rectangle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final int[][][] recSize = {new int[4][2]};
        final int[] x_grab = new int[1];
        final int[] y_grab = new int[1];
        final int[] width = new int[1];
        final int[] height = new int[1];
        final Mat[] frame = {new Mat()};
        final Mat[] outerBox = {new Mat()};
        final String filename = getFilesDir()+"detectHorse.jpg";
        final Mat imagecanny = new Mat();
        final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test2);
        final Bitmap bmp32 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        final Mat frame2 = new Mat();
        final Bitmap[] bitmapb = new Bitmap[1];
        final Bitmap[] bmp32b = new Bitmap[1];

        cutOut = (ImageView) findViewById(R.id.cutout);
        contours = (ImageView) findViewById(R.id.contours);
        rectangle = (DrawView) findViewById(R.id.rectangle);

        Button ok = (Button) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Rect> array = new ArrayList<>();
                ArrayList<Rect> arrayR = new ArrayList<>();


                recSize[0] = rectangle.getRectangle();
                Log.d(TAG, "point1("+recSize[0][0][0]+","+recSize[0][0][1]+")");
                Log.d(TAG, "point2("+recSize[0][1][0]+","+recSize[0][1][1]+")");
                Log.d(TAG, "point3("+recSize[0][2][0]+","+recSize[0][2][1]+")");
                Log.d(TAG, "point4("+recSize[0][3][0]+","+recSize[0][3][1]+")");

                x_grab[0] = recSize[0][0][0];

                y_grab[0] = recSize[0][0][1];
                for(int i=1; i<4; i++) {
                    if (recSize[0][i][0]<x_grab[0]) {
                        x_grab[0] = recSize[0][i][0];
                    }
                    if (recSize[0][i][1]<y_grab[0]) {
                        y_grab[0] = recSize[0][i][1];
                    }
                }
                width[0] = getLength(recSize[0][0][0], recSize[0][0][1], recSize[0][3][0], recSize[0][3][1]);
                height[0] = getLength(recSize[0][0][0], recSize[0][0][1], recSize[0][1][0], recSize[0][1][1]);

                Utils.bitmapToMat(bmp32, frame[0]);
                if (frame[0] == null)
                    Log.d("BitmapToMat", "Frame is empty");
                imag = frame[0].clone();
                Imgproc.cvtColor(frame[0], imag, Imgproc.COLOR_BGRA2BGR);
                Mat destination = new Mat();
                destination = frame[0].clone();

                List<MatOfPoint> contourDada = new ArrayList<MatOfPoint>();
                List<MatOfPoint> contourRef = new ArrayList<MatOfPoint>();

                destination = GrabCut(imag, x_grab[0], y_grab[0], width[0], height[0], contourDada);

                Imgcodecs.imwrite(filename, destination);
                Utils.matToBitmap(destination, bmp32);
                cutOut.setImageBitmap(bmp32);

                imag = null;


                bitmapb[0] = BitmapFactory.decodeFile(filename);
                bmp32b[0] = bitmapb[0].copy(Bitmap.Config.ARGB_8888, true);
                Utils.bitmapToMat(bmp32b[0], frame2);
                if (frame2 == null)
                    Log.d("BitmapToMat", "Frame is empty");
                imag = frame2.clone();
                outerBox[0] = new Mat(frame2.size(), CV_8UC1);
                Imgproc.cvtColor(frame2, outerBox[0], Imgproc.COLOR_BGR2GRAY);
                Imgproc.GaussianBlur(outerBox[0], outerBox[0], new Size(3, 3), 0);
                Imgproc.Canny(outerBox[0], imagecanny, 10, 100, 3, true);

                array = detection_contours(imagecanny, contourDada, imag);

                Bitmap refBmp = BitmapFactory.decodeResource(getResources(), R.drawable.dadablack);
                Bitmap refBmp32 = refBmp.copy(Bitmap.Config.ARGB_8888, true);
                Mat tmp = new Mat();
                Mat dada = new Mat();
                Mat reference = new Mat();
                Utils.bitmapToMat(refBmp32, tmp);
                dada = tmp.clone();
                Imgproc.cvtColor(tmp, dada, Imgproc.COLOR_BGRA2BGR);
                reference = tmp.clone();
                int widthR = getLength(12, 3, 12, 326);
                int heightR = getLength(12, 3, 401, 3);
                int xR = 12;
                int yR = 3;
                reference = GrabCut(dada, xR, yR, widthR, heightR, contourRef);
                outerBox[0] = new Mat(reference.size(), CV_8UC1);
                Imgproc.cvtColor(reference, outerBox[0], Imgproc.COLOR_BGR2GRAY);
                Imgproc.GaussianBlur(outerBox[0], outerBox[0], new Size(3, 3), 0);
                Imgproc.Canny(outerBox[0], imagecanny, 10, 100, 3, true);
                arrayR = detection_contours(imagecanny, contourRef, reference);

                boolean matching;
                for(int i=0; i<contourDada.size() && i<contourRef.size(); i++) {
                    Log.d(TAG, " --- matching index: "+i+" ---");
                    matching = getMatching(contourDada.get(i), contourRef.get(i), 0.01);
                }
                Log.d(TAG, "end matching");


                if (array.size() > 0) {

                    Iterator<Rect> it2 = array.iterator();
                    while (it2.hasNext()) {
                        Rect obj = it2.next();
                        Imgproc.rectangle(imag, obj.br(), obj.tl(), new Scalar(255, 0, 0), 1);

                        System.out.println("COORD = " + obj.x + "     " + obj.y + "\n");

                        System.out
                                .println("RECTANGLE 2 : " + obj.size() + "     " + obj.height + "     " + obj.width + "\n");
/*
                        double ref_metre = 2, ref_pxl = 360;

                        double distance = obj.height * ref_metre / ref_pxl;

                        System.out.println("Distance mesurée = " + distance + "\n" + "\n");

                        if (obj.x < 100) {
                            //longueur_ref_1m = obj.height;
                        }
*/


                    }
                }

                Utils.matToBitmap(imag, bmp32b[0]);
                contours.setImageBitmap(bmp32b[0]);

            }
        });
    }

    public Mat GrabCut(Mat source, int x_grab, int y_grab, int width, int height, List<MatOfPoint> contoursList) {

        Mat destination = new Mat();
        Mat bgdModel = new Mat();
        Mat fgdModel = new Mat();
        Mat masque = new Mat(1, 1, CvType.CV_8U, new Scalar(3));

        Rect rectangle = new Rect(x_grab, y_grab, width, height);


        Imgproc.grabCut(source, destination, rectangle, bgdModel, fgdModel, 8, Imgproc.GC_INIT_WITH_RECT);

        Core.compare(destination, masque, destination, Core.CMP_EQ);
        supp_petits_contours(destination);
        Mat foreground = new Mat();
        source.copyTo(foreground, destination);

        return foreground;
    }

    public static void supp_petits_contours(Mat source) {
        Mat v = new Mat();
        Mat vv = source.clone();

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        //vv needs to be single-channel
        Imgproc.findContours(vv, contours, v, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        Mat contour = null;
        double maxArea = 0;
        int idx;
        double contourarea;

        for (idx = 0; idx < contours.size(); idx++) {
            contour = contours.get(idx);
            contourarea = Imgproc.contourArea(contour);
            System.out.println("TAILLE : " + contourarea + " MAX : " + maxArea + "\n");

            if (contourarea > maxArea) {
                maxArea = contourarea;
            }

        }

        for (idx = 0; idx < contours.size(); idx++) {
            contour = contours.get(idx);
            contourarea = Imgproc.contourArea(contour);

            if (contourarea < maxArea) {
                Imgproc.drawContours(source, contours, idx, new Scalar(0, 0, 0), Core.FILLED);

            } else {
                Imgproc.drawContours(source, contours, idx, new Scalar(255, 255, 255), Core.FILLED);
            }
        }

        v.release();
    }

    public ArrayList<Rect> detection_contours(Mat outmat, List<MatOfPoint> contoursList, Mat image) {
        Mat v = new Mat();
        Mat vv = outmat.clone();

        Imgproc.findContours(vv, contoursList, v, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        Rect r = null;
        ArrayList<Rect> rect_array = new ArrayList<Rect>();

        for (int idx = 0; idx < contoursList.size(); idx++) {
            r = Imgproc.boundingRect(contoursList.get(idx));
            rect_array.add(r);
            Imgproc.drawContours(image, contoursList, idx, new Scalar(0, 255, 0));

        }

        v.release();

        return rect_array;
    }

    public static boolean getMatching(Mat contour1, Mat contour2, double ref) {
        double matching;

        matching = Imgproc.matchShapes(contour1, contour2, 1, 0.0);
        Log.d(TAG, "matching: "+matching);

        if (matching < ref) {
            return true;
        } else {
            return false;
        }
    }

    private double[] getColor(Mat source, int x, int y) {
        double[] rgb = source.get(x, y);

        return rgb;
    }

    public static MatOfPoint hull2Points(MatOfInt hull, MatOfPoint contour) {
        List<Integer> indexes = hull.toList();
        List<Point> points = new ArrayList<>();
        MatOfPoint point = new MatOfPoint();
        for (Integer index : indexes) {
            points.add(contour.toList().get(index));
        }
        point.fromList(points);
        return point;
    }

    public static Mat getHull(List<MatOfPoint> contours, Mat source) {
        MatOfInt hull = new MatOfInt();
        Mat hullMat = null;
        for (int i = 0; i < contours.size(); i++) {
            Imgproc.convexHull(contours.get(i), hull);
            MatOfPoint hullContour = hull2Points(hull, contours.get(i));
            Rect box = Imgproc.boundingRect(hullContour);
            hullMat = new Mat(source, box);

            String filename = "Imagehull.jpg";
            Imgcodecs.imwrite(filename, hullMat);
        }

        return hullMat;
    }

    public int getLength(int x1, int y1, int x2, int y2){
        return (int) ceil( sqrt( (pow((abs(x1-x2)),2)) + (pow((abs(y1-y2)),2)) ) );
    }

}
