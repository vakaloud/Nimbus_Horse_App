package com.nimbus.testgethorse;


/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

//Offset 9 top-left ?

import java.util.ArrayList;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class DrawView extends View {

    private static String TAG = "DrawView2";

    Point[] points = new Point[4];
    Point startMovePoint;

    /**
     * point1 and point 3 are of same group and same as point 2 and point4
     */
    int groupId = 2;
    private ArrayList<ColorBall> colorballs;
    // array that holds the balls
    private int balID = 0;
    // variable to know what ball is being dragged
    Paint paint;
    Canvas canvas;


    public DrawView(Context context) {
        super(context);
        init(context);
    }

    public DrawView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        paint = new Paint();
        setFocusable(true); // necessary for getting the touch events
        canvas = new Canvas();

        // setting the start point for the balls

        points[0] = new Point();
        points[0].x = 50;
        points[0].y = 20;

        points[1] = new Point();
        points[1].x = 150;
        points[1].y = 20;

        points[2] = new Point();
        points[2].x = 150;
        points[2].y = 120;

        points[3] = new Point();
        points[3].x = 50;
        points[3].y = 120;

        // declare each ball with the ColorBall class
        colorballs = new ArrayList<>();
        /*
        colorballs.add(0,new ColorBall(context, R.drawable.gray_circle, point1,0));
        colorballs.add(1,new ColorBall(context, R.drawable.gray_circle, point2,1));
        colorballs.add(2,new ColorBall(context, R.drawable.gray_circle, point3,2));
        colorballs.add(3,new ColorBall(context, R.drawable.gray_circle, point4,3));
        */
        colorballs.add(0, new ColorBall(context, canvas, points[0], 0));
        colorballs.add(1, new ColorBall(context, canvas, points[1], 1));
        colorballs.add(2, new ColorBall(context, canvas, points[2], 2));
        colorballs.add(3, new ColorBall(context, canvas, points[3], 3));


    }

    // the method that draws the balls
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int left, top, right, bottom;
        left = points[0].x;
        top = points[0].y;
        right = points[0].x;
        bottom = points[0].y;
        for (int i = 1; i < points.length; i++) {
            left = left > points[i].x ? points[i].x:left;
            top = top > points[i].y ? points[i].y:top;
            right = right < points[i].x ? points[i].x:right;
            bottom = bottom < points[i].y ? points[i].y:bottom;
        }

        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(5);

        //draw stroke
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.parseColor("#AADB1255"));
        paint.setStrokeWidth(2);
        canvas.drawRect(
                left + colorballs.get(0).getWidthOfBall() / 2,
                top + colorballs.get(0).getWidthOfBall() / 2,
                right + colorballs.get(2).getWidthOfBall() / 2,
                bottom + colorballs.get(2).getWidthOfBall() / 2, paint);
/*
        paint.setColor(Color.parseColor("#55DB1255"));
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);
        paint.setColor(Color.parseColor("#55FFFFFF"));
*/
        //fill the rectangle
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#55DB1255"));
        paint.setStrokeWidth(0);
        canvas.drawRect(
            left + colorballs.get(0).getWidthOfBall() / 2,
            top + colorballs.get(0).getWidthOfBall() / 2,
            right + colorballs.get(2).getWidthOfBall() / 2,
            bottom + colorballs.get(2).getWidthOfBall() / 2, paint);

        //BitmapDrawable mBitmap;
        //mBitmap = new BitmapDrawable();

        // draw the balls on the canvas

        for (ColorBall ball : colorballs) {
            //canvas.drawBitmap(ball.getBitmap(), ball.getX(), ball.getY(),
                    //new Paint());
            ball.drawBall();
        }

    }

    // events when touching the screen
    public boolean onTouchEvent(MotionEvent event) {
        int eventaction = event.getAction();

        int X = (int) event.getX();
        int Y = (int) event.getY();

        switch (eventaction) {

            case MotionEvent.ACTION_DOWN: // touch down so check if the finger is on
                // a ball
                balID = -1;
                startMovePoint = new Point(X,Y);
                for (ColorBall ball : colorballs) {
                    // check if inside the bounds of the ball (circle)
                    // get the center for the ball
                    int centerX = ball.getX() + ball.getWidthOfBall();
                    int centerY = ball.getY() + ball.getHeightOfBall();
                    paint.setColor(Color.CYAN);
                    // calculate the radius from the touch to the center of the ball
                    double radCircle = Math
                            .sqrt((double) (((centerX - X) * (centerX - X)) + (centerY - Y)
                                    * (centerY - Y)));

                    if (radCircle < ball.getWidthOfBall()) {

                        balID = ball.getID();
                        if (balID == 1 || balID == 3) {
                            groupId = 2;
                            canvas.drawRect(points[0].x, points[2].y, points[2].x, points[0].y,
                                    paint);
                        } else {
                            groupId = 1;
                            canvas.drawRect(points[1].x, points[3].y, points[3].x, points[1].y,
                                    paint);
                        }
                        invalidate();
                        break;
                    }
                    invalidate();
                }

                break;

            case MotionEvent.ACTION_MOVE: // touch drag with the ball
                // move the balls the same as the finger
                if (balID > -1) {
                    colorballs.get(balID).setX(X);
                    colorballs.get(balID).setY(Y);

                    paint.setColor(Color.CYAN);

                    if (groupId == 1) {
                        colorballs.get(1).setX(colorballs.get(0).getX());
                        colorballs.get(1).setY(colorballs.get(2).getY());
                        colorballs.get(3).setX(colorballs.get(2).getX());
                        colorballs.get(3).setY(colorballs.get(0).getY());
                        canvas.drawRect(points[0].x, points[2].y, points[2].x, points[0].y,
                                paint);
                    } else {
                        colorballs.get(0).setX(colorballs.get(1).getX());
                        colorballs.get(0).setY(colorballs.get(3).getY());
                        colorballs.get(2).setX(colorballs.get(3).getX());
                        colorballs.get(2).setY(colorballs.get(1).getY());
                        canvas.drawRect(points[1].x, points[3].y, points[3].x, points[1].y,
                                paint);
                    }

                    invalidate();
                }else{
                    if (startMovePoint!=null) {
                        paint.setColor(Color.CYAN);
                        int diffX = X - startMovePoint.x;
                        int diffY = Y - startMovePoint.y;
                        startMovePoint.x = X;
                        startMovePoint.y = Y;
                        colorballs.get(0).addX(diffX);
                        colorballs.get(1).addX(diffX);
                        colorballs.get(2).addX(diffX);
                        colorballs.get(3).addX(diffX);
                        colorballs.get(0).addY(diffY);
                        colorballs.get(1).addY(diffY);
                        colorballs.get(2).addY(diffY);
                        colorballs.get(3).addY(diffY);
                        if(groupId==1)
                            canvas.drawRect(points[0].x, points[2].y, points[2].x, points[0].y,
                                    paint);
                        else
                            canvas.drawRect(points[1].x, points[3].y, points[3].x, points[1].y,
                                    paint);
                        invalidate();
                    }
                }

                break;

            case MotionEvent.ACTION_UP:
                // touch drop - just do things here after dropping

                break;
        }
        // redraw the canvas
        invalidate();
        return true;

    }

    public void shade_region_between_points() {
        canvas.drawRect(points[0].x, points[2].y, points[2].x, points[0].y,
                paint);
    }

    public int[][] getRectangle (){
        int[][] rec = new int[4][2];
        rec[0][0]=colorballs.get(0).getX()+9;
        rec[0][1]=colorballs.get(0).getY()+9;
        rec[1][0]=colorballs.get(1).getX()+9;
        rec[1][1]=colorballs.get(1).getY()+9;
        rec[2][0]=colorballs.get(2).getX()+9;
        rec[2][1]=colorballs.get(2).getY()+9;
        rec[3][0]=colorballs.get(3).getX()+9;
        rec[3][1]=colorballs.get(3).getY()+9;

/*
        rec[0][0]=colorballs.get(0).getX();
        rec[0][1]=colorballs.get(0).getY();
        rec[1][0]=colorballs.get(1).getX();
        rec[1][1]=colorballs.get(1).getY();
        rec[2][0]=colorballs.get(2).getX();
        rec[2][1]=colorballs.get(2).getY();
        rec[3][0]=colorballs.get(3).getX();
        rec[3][1]=colorballs.get(3).getY();

        int[] origin = new int[2];
        getLocationOnScreen(origin);
        Log.d(TAG, "origin of view on screen: "+origin[0]+" "+origin[1]);
*/
        return rec;
    }

    public static class ColorBall {

        //Bitmap bitmap;
        Drawable d;
        Canvas mCanvas;
        Context mContext;
        Point point;
        int id;
        static int count = 0;

        //public ColorBall(Context context, int resourceId, Point point, int id) {
        public ColorBall(Context context, Canvas canvas, Point point, int id){
            this.id = id;
            //bitmap = BitmapFactory.decodeResource(context.getResources(),
                    //resourceId);
            d = context.getResources().getDrawable(R.drawable.rounded_corner);
            mContext = context;
            this.point = point;
            mCanvas = canvas;

        }

        public int getWidthOfBall() {
            //return bitmap.getWidth();
            return d.getIntrinsicWidth();
        }

        public int getHeightOfBall() {
            //return bitmap.getHeight();
            return d.getIntrinsicHeight();
        }
/*
        public Bitmap getBitmap() {
            return bitmap;
        }
*/
        public Drawable getD(){
            return d;
        }

        public int getX() {
            return point.x;
        }

        public int getY() {
            return point.y;
        }

        public int getID() {
            return id;
        }

        public void setX(int x) {
            point.x = x;
        }

        public void setY(int y) {
            point.y = y;
        }

        public void addY(int y){
            point.y = point.y + y;
        }

        public void addX(int x){
            point.x = point.x + x;
        }

        public void drawBall(){
            d.draw(mCanvas);
        }
    }
}
