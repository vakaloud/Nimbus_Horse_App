

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

public class DaDa_Detector {
	static 
	{
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	static Mat imag = null;

	public static void main(String[] args) {

		JFrame jframe = new JFrame("DaDa Detector");
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel vidpanel = new JLabel();
		jframe.setContentPane(vidpanel);
		jframe.setSize(640, 480);
		jframe.setVisible(true);

		Mat frame = new Mat();
		Mat outerBox = new Mat();

		ArrayList<Rect> array = new ArrayList<Rect>();
		Size sz = new Size(640, 480);		

		//int longueur_ref_1m = 0;
		String filename = "detectdef22.jpg";
		Mat imagecanny = new Mat();
		
		int x_grab = 45;
		int y_grab = 70;
		
		int width = 500;
		int height = 400;

		// --------------------------- PART 1 - GRABCUT / SMOOTHING ---------------------------------
		
		
				//Stream creation
				VideoCapture camera = new VideoCapture("Path/to/your/video");	
				
				//Read the video frame by frame
				if (camera.read(frame)) {
					Imgproc.resize(frame, frame, sz);
					imag = frame.clone();
					Mat destination = new Mat();
					
					//Cut Background
					destination = GrabCut(imag, x_grab, y_grab, width, height);

					//Get rid of small contours
					supp_small_contours(destination);

					Imgcodecs.imwrite(filename, destination);

					//Refresh window (considers all changes to destination)
					ImageIcon image = new ImageIcon(Mat2bufferedImage(destination));
					vidpanel.setIcon(image);
					vidpanel.repaint();
					
				}
		
		imag = null;
		
		// --------------------------- END PART 1 - GRABCUT / SMOOTHING ---------------------------------
		// --------------------------- PART 2 - CONTOURS ------------------------------------------------
		
		VideoCapture camera2 = new VideoCapture(filename);
		Mat frame2 = new Mat();
		
		if (camera2.read(frame2)) {
			Imgproc.resize(frame2, frame2, sz);
			imag = frame2.clone();

			outerBox = new Mat(frame2.size(), CvType.CV_8UC1);
			Imgproc.cvtColor(frame2, outerBox, Imgproc.COLOR_BGR2GRAY);
			Imgproc.GaussianBlur(outerBox, outerBox, new Size(3, 3), 0);
			Imgproc.Canny(outerBox, imagecanny, 10, 100, 3, true);
			// Imgproc.rectangle(imag, new Point(100,100), new Point(300,300), new Scalar(0, 255, 0));

			array = detection_contours(imagecanny);
			if (array.size() > 0) {

				Iterator<Rect> it2 = array.iterator();
				while (it2.hasNext()) {
					Rect obj = it2.next();
					Imgproc.rectangle(imag, obj.br(), obj.tl(), new Scalar(255, 0, 0), 1);
					System.out.println("COORD = " + obj.x + "     " + obj.y + "\n");

					System.out
							.println("RECTANGLE 2 : " + obj.size() + "     " + obj.height + "     " + obj.width + "\n");

					double ref_metre = 2, ref_pxl = 360;

					double distance = obj.height * ref_metre / ref_pxl;

					System.out.println("Distance mesur�e = " + distance + "\n" + "\n");

					if (obj.x < 100) {
						//longueur_ref_1m = obj.height;
					}
				}
			}
			
			
			Point originegrabcut = new Point(x_grab,y_grab);
			Point finrect = new Point(x_grab + width, y_grab + height);
			
			Imgproc.rectangle(imag, originegrabcut, finrect, new Scalar(0, 255, 255)); // JAUNE

			//Imgproc.rectangle(imag, new Point(30,30), new Point(600, 450), new Scalar(0, 255, 255));
			




			
			ImageIcon image = new ImageIcon(Mat2bufferedImage(imag));
			vidpanel.setIcon(image);
			vidpanel.repaint();
		}

		Imgcodecs.imwrite(filename, imagecanny);

	

		// Exemple getColor
		// System.out.println("red = " + getColor(imag, 200, 300)[2] + " green =
		// " + getColor(imag, 200, 300)[1] + " blue = " + getColor(imag, 200,
		// 300)[0] + "\n");

	}

	public static Mat GrabCut(Mat source, int x_grab, int y_grab, int width, int height) {

		Mat destination = new Mat();
		Rect rectangle = new Rect(x_grab, y_grab, width, height);	//	Area to which the algorithm is applied
		Mat bgdModel = new Mat();
		Mat fgdModel = new Mat();
		Mat masque = new Mat(1, 1, CvType.CV_8U, new Scalar(3));
		
		
		
		Imgproc.grabCut(source, destination, rectangle, bgdModel, fgdModel, 8, Imgproc.GC_INIT_WITH_RECT);
		
		//	All that is not foreground filled with black pixels
		Core.compare(destination, masque, destination, Core.CMP_EQ);
		Mat foreground = new Mat(source.size(), CvType.CV_8UC3, new Scalar(255, 255, 255));
		source.copyTo(foreground, destination);
		
		return destination;
	}
	
	public static void supp_small_contours(Mat outmat) {
		Mat v = new Mat();
		Mat vv = outmat.clone();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		
		//Find all contours and put them in the list "contours"
		Imgproc.findContours(vv, contours, v, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE); 

		Mat contour = null;
		double maxArea = 0;
		int idx;
		double contourarea;

		for (idx = 0; idx < contours.size(); idx++) {
			contour = contours.get(idx); //For each contour...
			contourarea = Imgproc.contourArea(contour);	//...We get its size
			System.out.println("TAILLE : " + contourarea + " MAX : " + maxArea + "\n");

			if (contourarea > maxArea) { //We keep the biggest
				maxArea = contourarea;
			}
			//If their are not too many details in the background and shadows and lights spots on the horse
			//The biggest contour should be of the horse
		}

		for (idx = 0; idx < contours.size(); idx++) {
			contour = contours.get(idx); // For each contour...
			contourarea = Imgproc.contourArea(contour);

			if (contourarea < maxArea) { // ...If it's smaller than the biggest contour, then fill the area with black pixels
				Imgproc.drawContours(outmat, contours, idx, new Scalar(0, 0, 0), Core.FILLED);

			} else { //...Otherwise fill it with white pixels
				Imgproc.drawContours(outmat, contours, idx, new Scalar(255, 255, 255), Core.FILLED);
			}
		}
		
		v.release();
	}
	
	public static ArrayList<Rect> detection_contours(Mat outmat) {
		Mat v = new Mat();
		Mat vv = outmat.clone();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Imgproc.findContours(vv, contours, v, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		Rect r = null;
		ArrayList<Rect> rect_array = new ArrayList<Rect>();

		for (int idx = 0; idx < contours.size(); idx++) {

			r = Imgproc.boundingRect(contours.get(idx));
			rect_array.add(r);
			Imgproc.drawContours(imag, contours, idx, new Scalar(0, 255, 0));

		}
		v.release();

		return rect_array;
	}
	
	public static BufferedImage Mat2bufferedImage(Mat image) {
		MatOfByte bytemat = new MatOfByte();
		Imgcodecs.imencode(".jpg", image, bytemat);
		byte[] bytes = bytemat.toArray();
		InputStream in = new ByteArrayInputStream(bytes);
		BufferedImage img = null;
		try {
			img = ImageIO.read(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}

	public static boolean getMatching(Mat contour1, Mat contour2, double ref) {
		double matching;

		matching = Imgproc.matchShapes(contour1, contour2, 1, 0.0);

		if (matching < ref) {
			return true;
		} else {
			return false;
		}
	}

	public static double[] getColor(Mat source, int x, int y) {
		double[] rgb = source.get(x, y);

		return rgb;
	}
	
	
	
	
	
	public static MatOfPoint hull2Points(MatOfInt hull, MatOfPoint contour) {
		List<Integer> indexes = hull.toList();
		List<Point> points = new ArrayList<>();
		MatOfPoint point = new MatOfPoint();
		for (Integer index : indexes) {
			points.add(contour.toList().get(index));
		}
		point.fromList(points);
		return point;
	}
	
	public static Mat getHull(List<MatOfPoint> contours, Mat source) {
		MatOfInt hull = new MatOfInt();
		Mat hullMat = null;
		for (int i = 0; i < contours.size(); i++) {
			Imgproc.convexHull(contours.get(i), hull);
			MatOfPoint hullContour = hull2Points(hull, contours.get(i));
			Rect box = Imgproc.boundingRect(hullContour);
			hullMat = new Mat(source, box);

			String filename = "Imagehull.jpg";
			Imgcodecs.imwrite(filename, hullMat);
		}

		return hullMat;
	}

	

	

}
