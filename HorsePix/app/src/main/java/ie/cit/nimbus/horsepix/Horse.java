package ie.cit.nimbus.horsepix;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * This class contains the absolute path to a picture taken in the app and data gotten from
 * processing the image, as well as the name of the horse that can be modified by the user
 */
public class Horse implements Serializable {
    private String horseName;
    private ArrayList<String> album;
    private ArrayList<Boolean> sent;
    private DateTime lastPhotoTime;

    //will contain all information about the horse (color, height...)

    public Horse(String horseName, String imgPath) {
        this.horseName = horseName;
        this.album = new ArrayList<>();
        this.album.add(imgPath);
        this.sent = new ArrayList<>();
        this.sent.add(false);
        lastPhotoTime = DateTime.now();
    }

    public String getName() {
        return horseName;
    }

    public void setName(String horseName) {
        this.horseName = horseName;
    }

    public ArrayList<String> getAlbum() {
        return album;
    }

    public void setAlbum(String imgPath, boolean sent) {
        lastPhotoTime = DateTime.now();
        this.album.add(imgPath);
        this.sent.add(sent);
    }

    public String getPath(int index) {
        return album.get(index);
    }
    //public void setPath(String imgPath) { this.imgPath = imgPath; }

    public ArrayList<Boolean> getSent() {
        return sent;
    }

    public boolean getPictureSent(int index) {
        return sent.get(index);
    }

    public void setPictureSent(int index, boolean sent) {
        this.sent.set(index, sent);
    }

    public DateTime getLastPhotoTime() {
        return lastPhotoTime;
    }

    public void setLastPhotoTime(DateTime lastPhotoTime) {
        this.lastPhotoTime = lastPhotoTime;
    }
}

