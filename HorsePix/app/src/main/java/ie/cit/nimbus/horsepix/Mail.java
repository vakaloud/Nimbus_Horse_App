package ie.cit.nimbus.horsepix;


import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class Mail {

    final String emailPort = "587";
    final String smtpAuth = "true";
    final String starttls = "false";
    final String emailHost = "mail.horsetech.ie";

    String fromEmail;
    String fromPassword;
    List<String> toEmailList;
    String emailSubject;
    String emailBody;
    ArrayList<String> emailAttachment;

    Properties emailProperties;
    Session mailSession;
    MimeMessage emailMessage;

    private Multipart _multipart;

    public Mail() {

    }

    public Mail(String fromEmail, String fromPassword,
                List<String> toEmailList, String emailSubject, String emailBody, ArrayList<String> filenames) {
        this.fromEmail = fromEmail;
        this.fromPassword = fromPassword;
        this.toEmailList = toEmailList;
        this.emailSubject = emailSubject;
        this.emailBody = emailBody;
        this._multipart = new MimeMultipart();
        this.emailAttachment = filenames;

        emailProperties = System.getProperties();
        emailProperties.put("mail.smtp.port", emailPort);
        emailProperties.put("mail.smtp.auth", smtpAuth);
        emailProperties.put("mail.smtp.starttls.enable", starttls);
        Log.i("Mail", "Mail server properties set.");
    }

    public MimeMessage createEmailMessage() throws AddressException,
            MessagingException, UnsupportedEncodingException {

        mailSession = Session.getDefaultInstance(emailProperties, null);
        emailMessage = new MimeMessage(mailSession);

        emailMessage.setFrom(new InternetAddress(fromEmail, fromEmail));
        for (String toEmail : toEmailList) {
            Log.i("Mail", "toEmail: " + toEmail);
            emailMessage.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(toEmail));
        }

        emailMessage.setSubject(emailSubject);
        //emailMessage.setContent(emailBody, "text/html");// for a html email
        // emailMessage.setText(emailBody);// for a text email
        try {
            addAttachment(emailAttachment, emailBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        emailMessage.setContent(_multipart);
        Log.i("Mail", "Email Message created.");
        return emailMessage;
    }

    public void sendEmail() throws AddressException, MessagingException {

        Transport transport = mailSession.getTransport("smtp");
        transport.connect(emailHost, fromEmail, fromPassword);
        Log.i("Mail", "allrecipients: " + Arrays.toString(emailMessage.getAllRecipients()));
        transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
        transport.close();
        Log.i("Mail", "Email sent successfully.");
    }

    public void addAttachment(ArrayList<String> filenames, String subject) throws Exception {
        int size = filenames.size();
        BodyPart[] messageBodyPartArray = new MimeBodyPart[size];
        DataSource[] sources = new DataSource[size];
        for (int i = 0; i < size; i++) {
            messageBodyPartArray[i] = new MimeBodyPart();
            sources[i] = new FileDataSource(filenames.get(i));
            messageBodyPartArray[i].setDataHandler(new DataHandler(sources[i]));
            messageBodyPartArray[i].setFileName(filenames.get(i));
            _multipart.addBodyPart(messageBodyPartArray[i]);
        }

        BodyPart messageBodyPart2 = new MimeBodyPart();
        messageBodyPart2.setText(subject);
        _multipart.addBodyPart(messageBodyPart2);
    }

}