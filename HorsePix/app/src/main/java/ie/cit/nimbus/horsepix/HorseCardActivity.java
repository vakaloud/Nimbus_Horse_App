package ie.cit.nimbus.horsepix;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.support.v4.content.FileProvider.getUriForFile;

/*
 * This activity displays the item clicked in full screen
 * You can rename the horse
 * You can send the card by email
 * You can delete the card permanently
 */
public class HorseCardActivity extends AppCompatActivity {
    public static final String TAG = "HorseCardActivity";
    private ArrayList<String> album;
    private int position = 0;
    private boolean deleted = false;
    private String hName;
    private TextView horseName;
    private EditText result;
    private ViewPager mViewPager;
    private ImageButton left;
    private ImageButton right;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);

        Intent intent = getIntent();

        // get horse's pictures
        album = intent.getStringArrayListExtra(HorseAdapter.TAG + "1");

        // get horse's info
        hName = intent.getStringExtra(HorseAdapter.TAG + "2");
        horseName = (TextView) findViewById(R.id.horse_name);
        horseName.setText(hName);

        // edit horse name
        ImageButton buttonEdit = (ImageButton) findViewById(R.id.edit_button);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog dialog = showRenameDialog();
                // we don't give access to the OK button until something is typed
                // so we don't return a null String
                dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
            }
        });

        // get position in ArrayList
        position = intent.getIntExtra(HorseAdapter.TAG + "3", -1);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //prepare sliding gallery and buttons
        left = (ImageButton) findViewById(R.id.left_arrow);
        right = (ImageButton) findViewById(R.id.right_arrow);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(new TouchImageAdapter());
        mViewPager.setCurrentItem(album.size() - 1);
        mViewPager.addOnPageChangeListener(onPageChangeListener);
        right.setVisibility(View.INVISIBLE);
        if (album.size() == 1)
            left.setVisibility(View.INVISIBLE);

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.arrowScroll(View.FOCUS_LEFT);

            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        });
    }

    // Custom dialog box so the user can rename the horse
    private AlertDialog showRenameDialog() {
        final ArrayList<Horse> mHorses = retrieveData();

        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.rename_dialog_box, null);

        //--Get the last 5 names used--
        TextView usedNames = v.findViewById(R.id.used_names);
        if (mHorses != null) {
            String names = null;
            for (int i = mHorses.size() - 1; i > -1 && i > (mHorses.size() - 6); i--) {
                if (i == (mHorses.size() - 1))
                    names = mHorses.get(i).getName();
                else
                    names = names + ", " + mHorses.get(i).getName();
            }
            usedNames.setText(names);
        } else {
            TextView usedText = v.findViewById(R.id.used_text);
            usedText.setVisibility(View.INVISIBLE);
            usedNames.setVisibility(View.INVISIBLE);
        }

        final TextView doublon = v.findViewById(R.id.doublon2);
        doublon.setVisibility(View.INVISIBLE);
        dlgAlert.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        OKCLicked();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        final AlertDialog dialog = dlgAlert.create();

        result = v.findViewById(R.id.editTextDialogUserInput);
        result.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // if something has been typed and is not used, the OK button is accessible
                boolean used = false;
                if (mHorses != null) {
                    for (int j = 0; j < mHorses.size(); j++) {
                        if (charSequence.toString().equalsIgnoreCase(mHorses.get(j).getName())) {
                            used = true;
                        }
                    }
                    if (used) {
                        String text = charSequence.toString() + " is already in use. Please choose another name.";
                        doublon.setText(text);
                        doublon.setVisibility(View.VISIBLE);
                        dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                    } else {
                        doublon.setVisibility(View.INVISIBLE);
                        dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                    }
                } else
                    dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dialog.show();
        return dialog;
    }

    private void OKCLicked() {
        String tmp = result.getText().toString();
        if (tmp != null) {
            hName = tmp;
            // the name is changed on the card when we close the dialog box
            horseName.setText(hName);
        }
    }

    @Override
    // prepare the results to return from the intent, here the name of the horse in case it was renamed
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        deleted = false;
        returnIntent.putExtra(TAG + "1", deleted);
        returnIntent.putExtra(TAG + "2", position);
        returnIntent.putExtra(TAG + "3", hName);
        setResult(RESULT_OK, returnIntent);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    // the actionbar displays a send button and a hidden menu that lists a delete action
    // hidden menu is preferable so that the user doesn't hit the button by accident
    // a dialog box to ask confirmation of deletion was added to avoid deleted by mistake
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_send) {
            ArrayList<Uri> uris = new ArrayList<Uri>();
            int weight = 0;
            int k = album.size() - 1;
            //convert from paths to Android friendly Parcelable Uri's
            while ((weight < 20) && (k > -1)) {
                File fileIn = new File(album.get(k));
                weight += (fileIn.length() / 1024 / 1024);
                if (weight < 20) {
                    Uri u = getUriForFile(getApplicationContext(), "ie.cit.nimbus.fileprovider", fileIn);
                    uris.add(u);
                }
                k--;
                Log.d(TAG, "dialogClosed: weight of attachments = " + weight + "MB");
            }

            //final Uri contentUri = getUriForFile(getApplicationContext(), "ie.cit.nimbus.fileprovider", picture);
            Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            intent.putExtra(Intent.EXTRA_SUBJECT, "[HorseTech for Science] " + hName)
                    .setType("image/jpg")
                    .putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris)
                    .setFlags(FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(intent, "Horse Passport"));

            return true;
        } else if (id == R.id.action_delete) {

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Deleting a Card")
                    .setMessage("Are you sure you want to delete this card?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            File pic;
                            for (int i = 0; i < album.size(); i++) {
                                pic = new File(album.get(i));
                                if (pic.exists()) {
                                    if (!pic.delete()) {
                                        deleted = false;
                                        Log.e(TAG, "Error while deleting pictures");
                                    } else
                                        deleted = true;
                                }
                            }
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra(TAG + "1", deleted);
                            returnIntent.putExtra(TAG + "2", position);
                            returnIntent.putExtra(TAG + "3", hName);
                            setResult(RESULT_OK, returnIntent);
                            finish();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();

            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Calculates the best ratio to use for a proportional thumbnail
    // it will give how many every pixel to read from the file to create the thumbnail
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    // This calls calculateInSampleSize to set options to decode the file to get a proportional
    // thumbnail without having to load the whole (heavy) picture first
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    // data.dat is read and information is deserialized and stored in an ArrayList<Horse>
    private ArrayList<Horse> retrieveData() {
        ArrayList<Horse> temp = new ArrayList<>();

        //get objects
        try {
            File f = new File(this.getFilesDir() + "/data.dat");
            if (f.exists()) {
                FileInputStream fis = getApplicationContext().openFileInput("data.dat");
                Log.i("retrieveData", "data.dat opened");
                ObjectInputStream ois = new ObjectInputStream(fis);
                temp = (ArrayList<Horse>) ois.readObject();
                Log.i("retrieveData", "Reading data.dat");
                for (Horse h : temp) {
                    Log.i("retrieveData", h.getName());
                }
                fis.close();
            } else
                return null;
        } catch (FileNotFoundException e) {
            Log.e("retrieveData", "Data File not found: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("retrieveData", "Error accessing Data file: " + e.getMessage());
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temp;
    }

    private class TouchImageAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return album.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            ImageView img = new ImageView(container.getContext());
            img.setImageDrawable(getImageFromSdCard(album.get(position)));
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return img;
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }


    }

    private Drawable getImageFromSdCard(String pathname) {
        Drawable d = null;
        try {
            int gallerySize;
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            if (size.x < size.y)
                gallerySize = size.x;
            else
                gallerySize = size.y;
            Bitmap bitmap = decodeSampledBitmapFromFile(pathname, gallerySize, gallerySize);
            d = new BitmapDrawable(bitmap);
        } catch (IllegalArgumentException e) {
            // TODO: handle exception
        }
        return d;
    }

    final ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (position == album.size() - 1) {
                right.setVisibility(View.INVISIBLE);
                left.setVisibility(View.VISIBLE);
            }

            // Hide left arrow if reach first position
            else if (position == 0) {
                left.setVisibility(View.INVISIBLE);
                right.setVisibility(View.VISIBLE);
            }

            // Else show both arrows
            else {
                left.setVisibility(View.VISIBLE);
                right.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}

