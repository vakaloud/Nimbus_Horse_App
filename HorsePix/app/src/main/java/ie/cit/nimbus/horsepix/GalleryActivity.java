package ie.cit.nimbus.horsepix;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;


/*
 * This activity starts when the user clicks on the gallery button in the Camera activity
 * It displays a back button on the action bar and sets up a RecyclerView using the custom
 * HorseAdapter
 */
public class GalleryActivity extends AppCompatActivity {
    public static final String TAG = "GalleryActivity";
    private RecyclerView mRecyclerView;
    private HorseAdapter mAdapter;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        context = getApplicationContext();

        mRecyclerView = (RecyclerView) findViewById(R.id.horse_recycler_view);
        mAdapter = new HorseAdapter(context, R.layout.category_list_item_1);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAdapter.onActivityResult(requestCode, resultCode, data);
    }
}

