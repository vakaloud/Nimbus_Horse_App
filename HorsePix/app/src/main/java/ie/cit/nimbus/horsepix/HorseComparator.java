package ie.cit.nimbus.horsepix;

import java.util.Comparator;

public class HorseComparator implements Comparator<Horse> {
    @Override
    public int compare(Horse o1, Horse o2) {
        if (o1.getLastPhotoTime().isAfter(o2.getLastPhotoTime())) {
            return -1;
        } else if (o1.getLastPhotoTime() == o2.getLastPhotoTime()) {
            return 0;
        } else {
            return 1;
        }
    }
}
