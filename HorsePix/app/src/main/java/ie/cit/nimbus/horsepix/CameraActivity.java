package ie.cit.nimbus.horsepix;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.media.ExifInterface;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.io.BufferedWriter;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * This class is the first activity called when the app is launched
 * It displays what the camera sees, an outline on top of it and two buttons (one to access the
 * gallery, the other to take a picture)
 * This activity is forced to landscape orientation
 * If a picture is taken, the picture and data from its processing are saved in data.dat in the
 * application's folder
 */
public class CameraActivity extends AppCompatActivity implements SensorEventListener {

    public static final String TAG = "Camera Activity";
    private Camera mCamera = null;
    private CameraPreview mPreview;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private RelativeLayout preview = null;
    private ImageView silhouette;
    private ImageButton galleryButton;
    private ImageButton captureButton;
    private ImageButton rotateButton;
    private RelativeLayout holdCapture = null;
    private RelativeLayout holdGallery = null;
    private RelativeLayout holdPosition = null;
    private RelativeLayout holdToggle = null;
    private RelativeLayout holdRotate = null;
    private TextView position;
    private SensorManager aSensorManager;
    private int correction = -1;
    private EditText result;
    private static String noName = "noName";
    private static int id = 0;
    private boolean sendForScience = true;
    private boolean headLeft = true;
    public String emailAddress = null;


    public CameraActivity() throws FileNotFoundException {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        try {
            File f = new File(getFilesDir() + "/dataMain.dat");
            if (f.exists()) {
                FileInputStream fis = getApplicationContext().openFileInput("dataMain.dat");
                byte[] dataMain = new byte[fis.available()];
                int check = fis.read(dataMain, 0, dataMain.length);
                String info = new String(dataMain);
                String[] splited = info.split(" ");
                sendForScience = Boolean.parseBoolean(splited[0]);
                id = Integer.parseInt(splited[1]);
                headLeft = Boolean.parseBoolean(splited[2]);
                emailAddress = splited[3];
                fis.close();
                Log.d(TAG, "Check email address: " + emailAddress);
            } else
                Log.e(TAG, "dataMain file does not exist.");
        } catch (FileNotFoundException e) {
            Log.e("retrieveData", "DataMAin File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e("retrieveData", "Error accessing DataMain file: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // This is to ensure that permission to take pictures and store on the device are permitted
        // before enabling anything.
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            permissionDialog();

        if (emailAddress == null) {
            new AlertDialog.Builder(this)
                    .setMessage("If you agree to share your horse pictures with HorseTech.ie, " +
                            "they will be sent to us by email." +
                            "\nDo you agree to give us your email address " +
                            "so we can keep you up to date with our project?")
                    .setPositiveButton("I agree", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final AlertDialog emailDialog = emailDialog();
                            emailDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                        }

                    })
                    .setNegativeButton("I disagree", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            emailAddress = "noAddress";
                        }
                    })
                    .show();
        }

        // Create an instance of Camera
        if (checkCameraHardware(this))
            mCamera = getCameraInstance();
        //mCamera = Camera.open();
        if (mCamera == null)
            Toast.makeText(this, "No camera available at the moment", Toast.LENGTH_LONG).show();

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        preview = (RelativeLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
        // Add the outline and buttons
        silhouette = (ImageView) findViewById(R.id.silhouette);
        if (headLeft)
            //silhouette.setImageResource(R.drawable.outline_red_b);
            silhouette.setImageResource(R.drawable.horse);
        else {
            //silhouette.setImageResource(R.drawable.outline_red);
            silhouette.setImageResource(R.drawable.horse);
            silhouette.setScaleX(-1);
        }


        holdCapture = (RelativeLayout) findViewById(R.id.hold_capture);
        holdGallery = (RelativeLayout) findViewById(R.id.hold_gallery);
        holdPosition = (RelativeLayout) findViewById(R.id.hold_position);
        holdPosition.setEnabled(false);
        holdToggle = (RelativeLayout) findViewById(R.id.hold_toggle);
        holdRotate = (RelativeLayout) findViewById(R.id.hold_rotate);

        rotateButton = (ImageButton) findViewById(R.id.button_rotate);
        rotateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headLeft = !headLeft;
                if (headLeft)
                    //silhouette.setImageResource(R.drawable.outline_red_b);
                    silhouette.setScaleX(-1);
                else
                    //silhouette.setImageResource(R.drawable.outline_red);
                    silhouette.setScaleX(1);
            }
        });

        captureButton = (ImageButton) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // The outline changes of color to show the user a picture is being taken
                if (headLeft)
                    //silhouette.setImageResource(R.drawable.outline_purple_b);
                    silhouette.setColorFilter(getResources().getColor(R.color.colorGreyToast));
                else
                    //silhouette.setImageResource(R.drawable.outline_purple);
                    silhouette.setColorFilter(getResources().getColor(R.color.colorGreyToast));


                // get an image from the camera
                mCamera.takePicture(null, null, mPicture);
            }
        });

        galleryButton = (ImageButton) findViewById(R.id.button_gallery);
        galleryButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            FileOutputStream fos = openFileOutput("dataMain.dat", Context.MODE_PRIVATE);
                            String info = sendForScience + " " + id + " " + headLeft + " " + emailAddress;
                            byte[] dataMain = info.getBytes();
                            fos.write(dataMain);
                            fos.close();
                        } catch (FileNotFoundException e) {
                            Log.e(TAG, "DataMain File not found: " + e.getMessage());
                        } catch (IOException e) {
                            Log.e(TAG, "Error accessing DataMain file: " + e.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // The gallery button starts a new activity
                        Intent intent_gallery = new Intent(getApplicationContext(), GalleryActivity.class);
                        startActivity(intent_gallery);
                    }
                }
        );

        aSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        position = (TextView) findViewById(R.id.position);

        SwitchCompat toggle = (SwitchCompat) findViewById(R.id.send_toggle_btn);
        toggle.setChecked(sendForScience);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    sendForScience = true;
                    sendMail();
                } else {
                    // The toggle is disabled
                    sendForScience = false;
                }
            }
        });

    }


    // Check if this device has a camera
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    // A safe way to get an instance of the Camera object.
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    // Take a picture, save it, create a Horse object, then restart the preview
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // The preview stops when the capture button is clicked. We wait 1 sec then restart
            SystemClock.sleep(1000);
            camera.startPreview();
            if (headLeft)
                //silhouette.setImageResource(R.drawable.outline_red_b);
                silhouette.setColorFilter(getResources().getColor(R.color.colorNimbus));

            else
                //silhouette.setImageResource(R.drawable.outline_red);
                silhouette.setColorFilter(getResources().getColor(R.color.colorNimbus));


            final AlertDialog dialog = showNameDialog(data);
            // we don't give access to the OK button until something is typed
            // so we don't return a null String
            dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
        }
    };

    // Create a File for saving an image or video
    // They will be stored in the application's folder
    // If the app is uninstalled, everything will be erased
    private File getOutputMediaFile(String hName) {
        File mediaStorageDir = new File(getFilesDir() + "/Pictures");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Pictures", "failed to create directory");
                return null;
            }
        }

        // Create a unique media file name using timestamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + hName + "_" + timeStamp + ".jpg");

        return mediaFile;
    }

    // Data is stored in the application's folder in a .dat file
    // An ArrayList<Horse> is serialized and stored in data.dat
    private boolean stockData(int position, String hName, String pathname) {
        ArrayList<Horse> mHorses = new ArrayList<>();

        try {
            File f = new File(getFilesDir() + "/data.dat");
            if (f.length() > 0) {
                // We retrieve the currently stored ArrayList
                ArrayList<Horse> tmp = retrieveData();
                if (tmp != null) {
                    Collections.sort(tmp, new HorseComparator());
                    mHorses = tmp;
                }
            }
            FileOutputStream fos = openFileOutput("data.dat", Context.MODE_PRIVATE);
            Horse newHorse = new Horse(hName, pathname);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            if (position > -1) {
                Log.d(TAG, "StockData: adding photo to known horse");
                mHorses.get(position).setAlbum(pathname, false);
                Log.d(TAG, "StockData: album :" + mHorses.get(position).getAlbum());
            } else {
                Log.d(TAG, "StockData: new horse");
                //Log.d(TAG,"pathname = "+pathname);
                //Log.d(TAG,"hName = "+hName);
                // We append the new Horse object
                mHorses.add(new Horse(hName, pathname));
            }
            /*
            // We append the new Horse object
            mHorses.add(newHorse);
            */
            // We overwrite data.dat with the updated ArrayList
            oos.writeObject(mHorses);
            fos.close();
            return true;

        } catch (FileNotFoundException e) {
            Log.d(TAG, "Data File not found: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.d(TAG, "Error accessing Data file: " + e.getMessage());
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    // data.dat is read and information is deserialized and stored in an ArrayList<Horse>
    private ArrayList<Horse> retrieveData() {
        ArrayList<Horse> temp = new ArrayList<>();

        //get objects
        try {
            File f = new File(this.getFilesDir() + "/data.dat");
            if (f.exists()) {
                FileInputStream fis = getApplicationContext().openFileInput("data.dat");
                Log.d("retrieveData", "data.dat opened");
                ObjectInputStream ois = new ObjectInputStream(fis);
                temp = (ArrayList<Horse>) ois.readObject();
                Log.d("retrieveData", "Reading data.dat");
                for (Horse h : temp) {
                    Log.d("retrieveData", h.getName());
                }
                fis.close();
            } else
                return null;
        } catch (FileNotFoundException e) {
            Log.e("retrieveData", "Data File not found: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("retrieveData", "Error accessing Data file: " + e.getMessage());
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temp;
    }

    private void getAccelerometer(SensorEvent event) {
        // alpha is calculated as t / (t + dT)
        // with t, the low-pass filter's time-constant
        // and dT, the event delivery rate

        float alpha = (float) 0.8;
        float[] gravity = new float[3];
        float[] linear_acceleration = new float[3];

        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];

        if ((linear_acceleration[0] > 5) && (linear_acceleration[1] < 0.5) && (linear_acceleration[1] > -0.5)
                && (linear_acceleration[2] < 0.5) && (linear_acceleration[2] > -0.5)) {
            if (correction != 0) {
                holdPosition.setEnabled(true);
                position.setText("Phone is straight");
                correction = 0;
                for (int i = 0; i < 1000; i++) {
                }
                holdPosition.setEnabled(false);

                //KEEP ONLY FOR SMART MODE
                /*
                captureButton.setEnabled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    captureButton.setBackground( getResources().getDrawable(R.drawable.round_background));
                } else{
                    captureButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.round_background) );

                }*/
            }
        } else if ((linear_acceleration[0] > 1) && (linear_acceleration[1] < -0.5) && (linear_acceleration[2] > -0.5)
                && (linear_acceleration[2] < 0.5)) {
            if (correction != 1) {
                holdPosition.setEnabled(true);
                position.setText("Phone is tilted to the left");
                correction = 1;
                for (int i = 0; i < 1000; i++) {
                }
                holdPosition.setEnabled(false);
                //KEEP ONLY FOR SMART MODE
                /*
                captureButton.setEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    captureButton.setBackground( getResources().getDrawable(R.drawable.round_background_disabled));
                } else{
                    captureButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.round_background_disabled) );

                }
                */
            }
        } else if ((linear_acceleration[0] > 1) && (linear_acceleration[1] > 0.5) && (linear_acceleration[2] > -0.5)
                && (linear_acceleration[2] < 0.5)) {
            if (correction != 2) {
                holdPosition.setEnabled(true);
                position.setText("Phone is tilted to the right");
                correction = 2;
                for (int i = 0; i < 1000; i++) {
                }
                holdPosition.setEnabled(false);
                //KEEP ONLY FOR SMART MODE
                /*
                captureButton.setEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    captureButton.setBackground( getResources().getDrawable(R.drawable.round_background_disabled));
                } else{
                    captureButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.round_background_disabled) );

                }
                */
            }
        } else if (linear_acceleration[2] > 1) {
            if (correction != 3) {
                holdPosition.setEnabled(true);
                position.setText("Phone is tilted downward");
                correction = 3;
                for (int i = 0; i < 1000; i++) {
                }
                holdPosition.setEnabled(false);
                //KEEP ONLY FOR SMART MODE
                /*
                captureButton.setEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    captureButton.setBackground( getResources().getDrawable(R.drawable.round_background_disabled));
                } else{
                    captureButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.round_background_disabled) );

                }
                */
            }
        } else if (linear_acceleration[2] < -1) {
            if (correction != 4) {
                holdPosition.setEnabled(true);
                position.setText("Phone is tilted upward");
                correction = 4;
                for (int i = 0; i < 1000; i++) {
                }
                holdPosition.setEnabled(false);
                //KEEP ONLY FOR SMART MODE
                /*
                captureButton.setEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    captureButton.setBackground( getResources().getDrawable(R.drawable.round_background_disabled));
                } else{
                    captureButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.round_background_disabled) );

                }
                */
            }
        } else {
            //KEEP ONLY FOR SMART MODE
            /*
            captureButton.setEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                captureButton.setBackground( getResources().getDrawable(R.drawable.round_background_disabled));
            } else{
                captureButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.round_background_disabled) );

            }
            */
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        for (int i = 0; i < 1000; i++) {
        }
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            getAccelerometer(event);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    // Custom dialog box so the user can rename the horse
    private AlertDialog showNameDialog(final byte data[]) {
        final int[] index = {-1};
        final ArrayList<String> names = new ArrayList<>();
        // We retrieve the currently stored ArrayList
        final ArrayList<Horse> mHorses = retrieveData();

        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.name_dialog_box, null);
        final RadioGroup rg = v.findViewById(R.id.radio_group);

        final TextView doublon = (TextView) v.findViewById(R.id.doublon);
        doublon.setVisibility(View.INVISIBLE);

        dlgAlert.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialogClosed(true, data, index[0]);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogClosed(false, data, index[0]);
                    }
                });
        final AlertDialog dialog = dlgAlert.create();

        if (mHorses != null) {
            Collections.sort(mHorses, new HorseComparator());
            for (int i = 0; i < mHorses.size(); i++) {
                names.add(mHorses.get(i).getName());
            }

            for (int i = 0; i < names.size(); i++) {
                RadioButton rb = new RadioButton(this); // dynamically creating RadioButton and adding to RadioGroup.
                rb.setText(names.get(i));
                rg.addView(rb, -1);
            }
            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int childCount = group.getChildCount();
                    for (int x = 0; x < childCount; x++) {
                        RadioButton btn = (RadioButton) group.getChildAt(x);
                        if (btn.getId() == checkedId) {
                            for (int i = 0; i < names.size(); i++) {
                                if (btn.getText().toString().equals(names.get(i)))
                                    index[0] = i;
                            }
                            dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }
                }
            });
        }

        result = v.findViewById(R.id.nameTheHorse);
        result.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                index[0] = -1;
                // if something has been typed and is not used, the OK button is accessible
                boolean used = false;
                if (mHorses != null) {
                    rg.clearCheck();
                    for (int j = 0; j < mHorses.size(); j++) {
                        if (charSequence.toString().equalsIgnoreCase(mHorses.get(j).getName())) {
                            used = true;
                        }
                    }
                    if (used) {
                        String text = charSequence.toString() + " is already in use. Please choose another name.";
                        doublon.setText(text);
                        doublon.setVisibility(View.VISIBLE);
                        dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                    } else {
                        doublon.setVisibility(View.INVISIBLE);
                        dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                    }
                } else
                    dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
        return dialog;
    }

    private void dialogClosed(boolean okClicked, byte data[], int index) {
        String hName = null;
        String pathname = null;

        // GET THE NAME
        if (okClicked) {
            if (index == -1) {
                String tmp = result.getText().toString();
                if (tmp != null && (!tmp.isEmpty()))
                    hName = tmp;
            } else {
                ArrayList<Horse> mHorses = retrieveData();
                if (mHorses != null) {
                    Collections.sort(mHorses, new HorseComparator());
                    hName = mHorses.get(index).getName();
                }
            }
        } else {
            id++;
            hName = noName + id;
        }
        File pictureFile = getOutputMediaFile(hName);
        if (pictureFile == null) {
            Log.e(TAG, "Error creating media file, check storage permissions: ");
            return;
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pathname = pictureFile.getAbsolutePath();

        // STOCK THE HORSE
        if (!stockData(index, hName, pathname)) {
            Log.e(TAG, "OKClicked: Error Data Storage");
            Toast.makeText(getApplicationContext(),
                    "An error occurred, your photo wasn't saved.",
                    Toast.LENGTH_SHORT).show();
        }

        sendMail();
    }

    private AlertDialog permissionDialog() {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        dlgAlert.setMessage("In order for the app to work correctly, you need to give permission:\n- to access the camera to take pictures;\n- to access your phone's memory so that the app can save your pictures in its personal folder only.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ActivityCompat.requestPermissions(CameraActivity.this,
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_CAMERA_PERMISSION);
                    }
                });
        final AlertDialog dialog = dlgAlert.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return dialog;
    }

    private AlertDialog emailDialog() {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.email_dialog_box, null);
        EditText email = v.findViewById(R.id.email);
        final EditText finalEmail = email;
        dlgAlert.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        emailAddress = finalEmail.getText().toString();
                    }
                });
        final AlertDialog dialog = dlgAlert.create();
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // if something has been typed and address format is valid, the OK button is accessible
                if (isEmailValid(charSequence.toString()))
                    dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                else
                    dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
        return dialog;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void sendMail() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            ArrayList<String> unsent = new ArrayList<>();
            ArrayList<String> attachments;
            ArrayList<Horse> mHorses = retrieveData();
            long weight = 0;

            if (mHorses != null) {
                //get all unsent pictures
                for (int i = 0; i < mHorses.size(); i++) {
                    attachments = mHorses.get(i).getAlbum();
                    for (int j = 0; j < mHorses.get(i).getSent().size(); j++) {
                        if (!mHorses.get(i).getSent().get(j)) {
                            unsent.add(attachments.get(j));
                        }
                    }
                }

                Log.d(TAG, "dialogClosed: nb unsent = " + unsent.size());

                //prepare email under 20MB
                while (unsent.size() > 0) {



                    attachments = new ArrayList<>();
                    int k = unsent.size() - 1;


                    //add pictures to email
                    while ((weight < 20) && (k > -1)) {

                        // attach image
                        File file = new File(unsent.get(k));
                        attachments.add(file.getAbsolutePath());
                        unsent.remove(k);
                        weight += (file.length() / 1024 / 1024);

                        // create .yml file to hold meta data

                        String filename_no_ext = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf('.'));
                        File meta_attach = new File(filename_no_ext + ".yml");

                        try  {

                            PrintWriter outWriter = new PrintWriter( new PrintStream(meta_attach));


                            outWriter.println("---");
                            outWriter.println("  application: HorsePix");
                            outWriter.println("  filename: "+filename_no_ext);
                            //TODO: add md5 hash of file
                            outWriter.println("  user_email: "+emailAddress);

                            // getting horse name from name of image - this is a hack for now
                            String[] parts = filename_no_ext.substring(filename_no_ext.lastIndexOf('/')+1).split("_");
                            outWriter.println("  what_type: horse");
                            outWriter.println("  what_handle: " + parts[1]);
                            outWriter.println(String.format("  when: %s-%s-%sT%s:%s:%s",
                                    parts[2].substring(0,4),
                                    parts[2].substring(4,6),
                                    parts[2].substring(6,8),
                                    parts[3].substring(0,2),
                                    parts[3].substring(2,4),
                                    parts[3].substring(4,6)));



                            outWriter.close();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        attachments.add(meta_attach.getAbsolutePath());


                        k--;

                    }

                    Log.d(TAG, "dialogClosed: nb attachments = " + attachments.size());

                    //send email
                    String fromEmail = "pic@horsetech.ie";
                    String fromPassword = "Valegro_35";
                    String toEmails = "pic@horsetech.ie," + this.emailAddress;
                    List<String> toEmailList = Arrays.asList(toEmails
                            .split("\\s*,\\s*"));
                    Log.i("SendMailActivity", "To List: " + toEmailList);
                    String emailSubject = "[HorsePix]";
                    String emailBody = "Images from HorsePix App.";
                    new SendMailTask((Activity) this).execute(fromEmail,
                            fromPassword, toEmailList, emailSubject, emailBody, attachments);

                    //check picture as sent
                    for (int i = 0; i < mHorses.size(); i++) {
                        for (int j = 0; j < mHorses.get(i).getSent().size(); j++) {
                            for (k = 0; k < attachments.size(); k++) {
                                if (attachments.get(k).equals(mHorses.get(i).getPath(j))) {
                                    mHorses.get(i).setPictureSent(j, true);
                                }
                            }
                        }
                    }

                    //store changed data
                    try {
                        FileOutputStream fos3 = openFileOutput("data.dat", Context.MODE_PRIVATE);
                        ObjectOutputStream oos3 = new ObjectOutputStream(fos3);
                        oos3.writeObject(mHorses);
                        fos3.close();
                    } catch (FileNotFoundException e) {
                        Log.e(TAG, "Data File not found: " + e.getMessage());
                    } catch (IOException e) {
                        Log.e(TAG, "Error accessing Data file: " + e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage("An internet connection is needed to send your pictures to HorseTech." +
                            "\nPlease check your connection and try again.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }

                    })
                    .show();
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        try {
            File f = new File(getFilesDir() + "/dataMain.dat");
            if (f.exists()) {
                FileInputStream fis = getApplicationContext().openFileInput("dataMain.dat");
                byte[] dataMain = new byte[fis.available()];
                int check = fis.read(dataMain, 0, dataMain.length);
                String info = new String(dataMain);
                String[] splited = info.split(" ");
                sendForScience = Boolean.parseBoolean(splited[0]);
                id = Integer.parseInt(splited[1]);
                headLeft = Boolean.parseBoolean(splited[2]);
                emailAddress = splited[3];
                fis.close();
            } else
                Log.e(TAG, "dataMain file does not exist.");
        } catch (FileNotFoundException e) {
            Log.e("retrieveData", "DataMAin File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e("retrieveData", "Error accessing DataMain file: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        aSensorManager.registerListener(this,
                aSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

        try {
            Log.i("TEST onresume", "onResume Running");
            mCamera = Camera.open();
            Log.i("TEST onresume", "camera " + mCamera);
            mCamera.setPreviewCallback(null);
            Log.i("TEST onresume", "camera " + mCamera);
            mPreview = new CameraPreview(this, mCamera);
            Log.i("TEST onresume", "camera " + mCamera);
            preview.addView(mPreview);
            Log.i("TEST onresume", "camera " + mCamera);
            mCamera.startPreview();
        } catch (Exception e) {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
        Log.i("TEST onresume", "camera " + mCamera);
        // To ensure that all small elements are visible over the preview
        silhouette.bringToFront();
        holdGallery.bringToFront();
        holdCapture.bringToFront();
        holdPosition.bringToFront();
        holdPosition.setEnabled(true);
        holdToggle.bringToFront();
        holdRotate.bringToFront();

    }

    @Override
    protected void onPause() {
        super.onPause();


        try {
            FileOutputStream fos = openFileOutput("dataMain.dat", Context.MODE_PRIVATE);
            String info = sendForScience + " " + id + " " + headLeft + " " + emailAddress;
            byte[] dataMain = info.getBytes();
            fos.write(dataMain);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "DataMain File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Error accessing DataMain file: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        aSensorManager.unregisterListener(this);
        holdPosition.setEnabled(false);
        releaseCamera();              // release the camera immediately on pause event
        Log.e(TAG, "onPause called");

    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

}
