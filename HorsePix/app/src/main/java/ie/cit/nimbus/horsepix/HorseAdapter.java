package ie.cit.nimbus.horsepix;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/*
 * This adapter is set up in the gallery activity
 * The RecyclerView asks the adapter to form a list of items created from the data contained in
 * an ArrayList<Horse>
 * This ArrayList gets its objects from deserializing a byte array filled after reading data.dat,
 * a file stored in the applications folder containing data on processed images by the app
 */
public class HorseAdapter extends RecyclerView.Adapter<HorseAdapter.HorseHolder> {

    public static final String TAG = "HorseAdapter";
    private ArrayList<Horse> mHorses;
    private int itemLayout;
    private static Context context;
    private Activity activity;
    public static final int REQUEST_MODIFICATION = 1;

    public HorseAdapter(Context context, int itemLayout) {
        this.context = context;
        this.itemLayout = itemLayout;
        mHorses = retrieveData();
        if (mHorses == null) {
            Log.e("Init", "Error initializing mHorses");
        }
    }

    @Override
    public HorseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        activity = (Activity) parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new HorseHolder(v);
    }

    @Override
    public int getItemCount() {
        return mHorses.size();
    }

    @Override
    public void onBindViewHolder(HorseHolder holder, int positionIndex) {
        final int position = positionIndex;
        final Horse h = mHorses.get(position);
        holder.bindData(h);

        // If we click on an item, then we start a new activity displaying all data about it
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_card = new Intent(context, HorseCardActivity.class);
                intent_card.putExtra(TAG + "1", h.getAlbum());
                intent_card.putExtra(TAG + "2", h.getName());
                intent_card.putExtra(TAG + "3", position);
                intent_card.setFlags(0);
                activity.startActivityForResult(intent_card, REQUEST_MODIFICATION);

            }
        });
    }

    /*
     * This class' constructor links Horse data to the XML file used to display it as an item
     * in the list
     */
    public static class HorseHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mNameTextView;

        public HorseHolder(final View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageView);
            mNameTextView = itemView.findViewById(R.id.textview_name);
        }

        public void bindData(Horse h) {
            // You create a thumbnail of the image
            Bitmap bmp = decodeSampledBitmapFromFile(h.getPath(h.getAlbum().size() - 1), 80, 80);
            mImageView.setImageBitmap(bmp);
            mNameTextView.setText(h.getName());
        }
    }

    /*
     * You take care of all major changes after returning from a HorseCardActivity
     * When you come back, there are three options
     * 1- you renamed the horse: the ArrayList and data.dat have to be updated, the view reloaded
     * 2- you deleted a card: the ArrayList and data.dat have to be updated, the view reloaded
     * 3- you read the card and turned back without modifying anything: do nothing
     */
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {
        int position;
        boolean deleted;
        String newName;
        Log.i("HorseCardResult", "onActivityResult called");
        if (requestCode == REQUEST_MODIFICATION) {
            if (resultCode == RESULT_OK) {
                deleted = data.getBooleanExtra(HorseCardActivity.TAG + "1", false);
                Log.i("HorseCardResult", "deleted=" + deleted);
                if (deleted) {
                    position = data.getIntExtra(HorseCardActivity.TAG + "2", -1);
                    Log.i("HorseCardResult", "so view " + position + " removed");
                    String name = mHorses.get(position).getName();
                    // remove the Horse object from the ArrayList
                    mHorses.remove(position);
                    // rewrite data.dat
                    if (!stockData())
                        Log.e("HorseCardResult", "Error Stocking Changed Data");
                    else {
                        // reload the RecyclerView to stop displaying the item
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, mHorses.size());
                        Toast.makeText(context, "Removed : " + name, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    newName = data.getStringExtra(HorseCardActivity.TAG + "3");
                    position = data.getIntExtra(HorseCardActivity.TAG + "2", -1);
                    // change horse name in the ArrayList and the file
                    mHorses.get(position).setName(newName);
                    if (!stockData())
                        Log.d("HorseCardResult", "Error Stocking Changed Data");
                    else
                        // reload the view to display with new name
                        notifyItemChanged(position);
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("HorseCardResult", "no modification made");
            }
        }
    }

    // Calculates the best ratio to use for a proportional thumbnail
    // it will give how many every pixel to read from the file to create the thumbnail
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    // This calls calculateInSampleSize to set options to decode the file to get a proportional
    // thumbnail without having to load the whole (heavy) picture first
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    // This reads data.dat to fill a byte array that is deserialized to fill a corresponding
    // ArrayList<Horse>
    private ArrayList<Horse> retrieveData() {
        ArrayList<Horse> temp = new ArrayList<>();

        //get objects
        try {
            File f = new File(context.getFilesDir() + "/data.dat");
            if (f.exists()) {
                FileInputStream fis = context.openFileInput("data.dat");
                Log.i("retrieveData", "data.dat opened");
                ObjectInputStream ois = new ObjectInputStream(fis);
                temp = (ArrayList<Horse>) ois.readObject();
                Log.i("retrieveData", "Reading data.dat");
                for (Horse h : temp) {
                    Log.i("retrieveData", h.getName());
                }
                fis.close();
            } else
                Toast.makeText(context, "Your gallery is empty. Take a picture first", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            Log.e("retrieveData", "Data File not found: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("retrieveData", "Error accessing Data file: " + e.getMessage());
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temp;
    }

    // this serializes mHorses into a byte array that is then written in data.dat
    private boolean stockData() {
        try {
            FileOutputStream fos = context.openFileOutput("data.dat", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(mHorses);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("StockChangedData", "Data File not found: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.e("StockChangedData", "Error accessing Data file: " + e.getMessage());
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

}
